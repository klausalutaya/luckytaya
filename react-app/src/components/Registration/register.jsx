import React, { useState, useEffect, useRef } from "react";
import "./register.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEyeSlash, faEye } from '@fortawesome/free-solid-svg-icons';
import arrowLeftIcon from './arrow-left-2.svg';
import camera from './camera.png'
import babae from './Ellipse 43.png'
import Modal from 'react-modal';
import Swal from 'sweetalert2';

const Header = ({ handlePrevPage }) => {
  return (
    <header className="main-header">
      <div className="inner-header">
        <div className="header-icons" onClick={handlePrevPage}>
          <img src={arrowLeftIcon} alt="Back" className="arrow-icon" style={{ fill: 'white' }} />
        </div>
      </div>
    </header>
  );
};

Modal.setAppElement('#root');
const NewPage = ({ handleNextPage, handlePrevPage }) => {
  const [isModalOpen, setIsModalOpen] = useState(false); // State to control the visibility of the modal

  const goBackToCase3 = () => {
    handlePrevPage(3);
  };

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  return (
    <>
      <form>
        <h1 className="register__title2">FACE VERIFICATION</h1>
        <img className="babae" src={babae} alt="" />
        <h3 className="selfie">Take a selfie to verify your account</h3>
        <p className="facial">Your facial Information will be collected through the verification process.</p>
      </form>
      <div className="previous-button" onClick={goBackToCase3}>
        <img src={arrowLeftIcon} alt="Previous" className="arrow-icon" style={{ fill: 'white' }} />
      </div>
      <button className="buttonselfie button--flex" onClick={openModal} id="nextButton">
        TAKE A PICTURE
      </button>

      <Modal
        isOpen={isModalOpen}
        onRequestClose={closeModal}
        contentLabel="Pop-up Page"
        className="modal"
        overlayClassName="overlay"
      >
        {/* Your pop-up page content goes here */}
        <h2>Pop-up Page</h2>
        <p>This is the content of the pop-up page.</p>

        <button className="close-button" onClick={closeModal}>
          Close
        </button>
      </Modal>
    </>
  );
};

const Register = () => {
  const [showPassword, setShowPassword] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const ellipses = useRef([]);
  const [showNewForm, setShowNewForm] = useState(false);

  const handleNextPage = () => {
    if (currentPage < 4) {
      setCurrentPage(currentPage => currentPage + 1);
    }
  };

  useEffect(() => {
    const ellipsesList = ellipses.current;
    const nextButton = document.getElementById('nextButton');

    const handleClick = () => {
      if (currentPage < ellipsesList.length - 1 && ellipsesList[currentPage]) {
        ellipsesList[currentPage].classList.remove('active');
        setCurrentPage(currentPage => currentPage + 1);
      }
    };

    nextButton.addEventListener('click', handleClick);

    return () => {
      nextButton.removeEventListener('click', handleClick);
    };
  }, [currentPage]);

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  const handlePrevPage = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  };


  const handleBoxClick = () => {
    setShowNewForm(true);
  };

  const renderForm = () => {
    if (showNewForm) {
      return <NewPage handleNextPage={handleNextPage} handlePrevPage={handlePrevPage} />;
    }
    switch (currentPage) {
      case 1:
        return (
          <>
            <h1 className="register__title1">REGISTRATION</h1>

            <div className="progress-bar">
              <div className="progress-bar-line"></div>
              <div className={`progress-bar-ellipse ${currentPage === 1 ? 'active' : ''}`} ref={ref => ellipses.current[0] = ref}></div>
              <div className="progress-bar-ellipse"></div>
              <div className="progress-bar-ellipse"></div>
            </div>

            <h3 className="register__title">Account and Contact Details</h3>

            <form>
              <div className="register__form-div">
                <label className="register__form-tag">Username</label>
                <input
                  type="text"
                  name="name"
                  className="register__form-input"
                  placeholder="Type here"
                />
              </div>

              <div className="register__form-div password-input-container">
                <label className="register__form-tag">Password</label>
                <input
                  type={showPassword ? 'text' : 'password'}
                  name="password"
                  className="register__form-input"
                  placeholder="Type here"
                />
                <div className="password-toggle" onClick={togglePasswordVisibility}>
                  <FontAwesomeIcon
                    icon={showPassword ? faEye : faEyeSlash}
                    className="password-icon"
                  />
                </div>
              </div>

              <div className="register__form-div password-input-container">
                <label className="register__form-tag">Confirm Password</label>
                <input
                  type={showPassword ? 'text' : 'password'}
                  name="confirmPassword"
                  className="register__form-input"
                  placeholder="Type here"
                />
                <div className="password-toggle" onClick={togglePasswordVisibility}>
                  <FontAwesomeIcon
                    icon={showPassword ? faEye : faEyeSlash}
                    className="password-icon"
                  />
                </div>
              </div>

              <div className="register__form-div">
                <label className="register__form-tag">Email Address</label>
                <input
                  type="email"
                  name="email"
                  className="register__form-input"
                  placeholder="Type here"
                />
              </div>

              <div className="register__form-div">
                <label className="register__form-tag">Mobile Number</label>
                <input
                  type="tel"
                  name="phone"
                  className="register__form-input"
                  placeholder="+63"
                />
              </div>
            </form>

            <button className="button__ button--flex" onClick={handleNextPage} id="nextButton">
              NEXT
            </button>
          </>
        );
      case 2:
        return (
          <>
            <h1 className="register__title1">REGISTRATION</h1>

            <div className="progress-bar">
              <div className="progress-bar-line"></div>
              <div className={`progress-bar-ellipse ${currentPage === 1 ? 'active' : ''}`} ref={ref => ellipses.current[0] = ref}></div>
              <div className={`progress-bar-ellipse ${currentPage === 2 ? 'active' : ''}`} ref={ref => ellipses.current[1] = ref}></div>
              <div className="progress-bar-ellipse"></div>
            </div>

            <div className="previous-button" onClick={handlePrevPage}>
              <img src={arrowLeftIcon} alt="Previous" className="arrow-icon" style={{ fill: 'white' }} />
            </div>

            <form>
              <h3 className="register__title4">Basic Information</h3>

              <div className="register__form-div">
                <label className="register__form-tag">First Name</label>
                <input
                  type="text"
                  name="basicUsername"
                  className="register__form-input"
                  placeholder="Type here"
                />
              </div>

              <div className="register__form-div">
                <label className="register__form-tag">Middle Name</label>
                <input
                  type="text"
                  name="basicPassword"
                  className="register__form-input"
                  placeholder="Type here"
                />
              </div>

              <div className="register__form-div">
                <label className="register__form-tag">Last Name</label>
                <input
                  type="text"
                  name="basicConfirmPassword"
                  className="register__form-input"
                  placeholder="Type here"
                />
              </div>

              <div className="register__form-div">
                <label className="register__form-tag">Birthdate</label>
                <input
                  type="date"
                  name="basicEmail"
                  className="register__form-input"
                  placeholder="Select Birthdate"
                />
              </div>

              <h3 className="register__title3">Address</h3>

              <div className="register__form-div">
                <label className="register__form-tag"></label>
                <input
                  type="text"
                  name="region"
                  className="register__form-input1"
                  placeholder="Select Region"
                />
              </div>

              <div className="register__form-div">
                <label className="register__form-tag"></label>
                <input
                  type="text"
                  name="city"
                  className="register__form-input1"
                  placeholder="Select City"
                />
              </div>

              <div className="register__form-div">
                <label className="register__form-tag"></label>
                <input
                  type="text"
                  name="barangay"
                  className="register__form-input1"
                  placeholder="Select Barangay"
                />
              </div>

              <div className="register__form-div">
                <label className="register__form-tag">Home Number and Street Name</label>
                <input
                  type="text"
                  name="homeNumber"
                  className="register__form-input1"
                  placeholder="Type here"
                />
              </div>


            </form>

            <button className="button__ button--flex" onClick={handleNextPage} id="nextButton">
              NEXT
            </button>
          </>
        );
      case 3:
        return (
          <>
            <h1 className="register__title1">REGISTRATION</h1>

            <div className="progress-bar">
              <div className="progress-bar-line"></div>
              <div className={`progress-bar-ellipse ${currentPage === 1 ? 'active' : ''}`} ref={ref => ellipses.current[0] = ref}></div>
              <div className={`progress-bar-ellipse ${currentPage === 2 ? 'active' : ''}`} ref={ref => ellipses.current[1] = ref}></div>
              <div className={`progress-bar-ellipse ${currentPage === 3 ? 'active' : ''}`} ref={ref => ellipses.current[2] = ref}></div>
            </div>

            <div className="previous-button" onClick={handlePrevPage}>
              <img src={arrowLeftIcon} alt="Previous" className="arrow-icon" style={{ fill: 'white' }} />
            </div>

            <form>
              <p className="register__p">
                You are required to upload at least one (1) valid ID. Please use only small files (less than 300kb) and short file names.
              </p>
              <p className="register__p1">
                For files bigger than 300kb, please send your valid ID to our 24/7 Customer Support via{' '}
                <span style={{ color: '#F0775D' }}>Facebook</span> {' '}
                <span style={{ color: '#F0775D' }}>Messenger</span>.
              </p>
              <div className="register__form-div">
                <label className="register__form-tag1"></label>
                <div className="custom-dropdown">
                  <button className="dropdown-btn"></button>
                  <div className="dropdown-content">
                    <select name="phone" className="register__form-input1">
                      <option value="id1">SSS</option>
                      <option value="id2">PhilHealth</option>
                      <option value="id3">UMID</option>
                      <option value="id4">Passport</option>
                      <option value="id5">TIN</option>
                      <option value="id6">PRC</option>
                    </select>
                  </div>
                </div>
              </div>

              <div className="register__form-div">
                <label className="register__form-tag1">ID Number</label>
                <input
                  type="text"
                  name="name"
                  className="register__form-input1"
                  placeholder="Type here"
                />
              </div>

              <div className="register__form-div">
                <label className="register__form-tag1">ID Image Upload</label>
                <div className="register__form-input1-container">
                  <input
                    type="text"
                    name="name"
                    className="register__form-input1"
                    placeholder="Type here"
                  />
                  <button className="register__form-upload-button" onClick={() => {
                    Swal.fire({
                      title: "Success!",
                      text: "The photo has been successfully uploaded!",
                      icon: "success",
                      confirmButtonText: "OK"
                    });
                  }}>UPLOAD</button>
                </div>
              </div>

              <div className="box" onClick={handleBoxClick}>
                <div className="icon">
                  <img src={camera} alt="Camera" />
                </div>
                <div className="text">Please take a picture.</div>
              </div>

              <h1 className="legal__">Legal Agreements</h1>
              <div className="frame-306">
                <label className="checkbox-container">
                  <input type="checkbox" className="checkbox-input" />
                  <span className="checkbox-checkmark"></span>
                </label>
                <p className="text5">
                  I am at least 21 years old and have read and accepted the{' '}
                  <span style={{ color: '#F0775D', textDecoration: 'underline' }}>Terms and</span>{' '}
                  <span style={{ color: '#F0775D', textDecoration: 'underline' }}>Conditions.</span>
                </p>
              </div>

              <div className="frame-306">
                <label className="checkbox-container">
                  <input type="checkbox" className="checkbox-input" />
                  <span className="checkbox-checkmark"></span>
                </label>
                <p className="text6">I confirm I am NOT a Philippine Government Employee.</p>
              </div>

              <div className="frame-306">
                <label className="checkbox-container">
                  <input type="checkbox" className="checkbox-input" />
                  <span className="checkbox-checkmark"></span>
                </label>
                <p className="text7">
                  I am aware that this site uses cookies. By continuing to use this site, I agree to the use of cookies and its{' '}
                  <span style={{ color: '#F0775D', textDecoration: 'underline' }}>Privacy</span>{' '}
                  <span style={{ color: '#F0775D', textDecoration: 'underline' }}>Policy.</span>
                </p>
              </div>

              <h3 className="note__">Note: Clicking Create Account will launch a video call with our Customer Support for identity verification.</h3>
            </form>

            <button className="button___ button--flex" onClick={handleNextPage} id="nextButton">
              CREATE ACCOUNT
            </button>
          </>
        );
    }
  };

  return (
    <div className="register__content">
      <Header handlePrevPage={handlePrevPage} />
      {renderForm()}
    </div>
  );
};

export default Register;
