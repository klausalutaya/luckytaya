import React from "react";
import "./register.css";
import arrowLeftIcon from './arrow-left-2.svg';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Header = () => {
    return (
      <header className="main-header">
        <div className="inner-header">
          <div className="header-icons">
            <img src={arrowLeftIcon} alt="Back" className="arrow-icon" style={{ fill: 'white' }} />
          </div>
        </div>
      </header>
    );
  };
const Register = () => {
  return (
    <div className="contact__content">
      <Header />

      <h1 className="contact__title1">REGISTRATION</h1>

      <div className="progress-bar">
        <div className="progress-bar-line"></div>
        <div className="progress-bar-ellipse active"></div>
        <div className="progress-bar-ellipse"></div>
        <div className="progress-bar-ellipse"></div>
      </div>


      <h3 className="contact__title">Account and Contact Details</h3><form>
        <div className="contact__form-div">
          <label className="contact__form-tag">Username</label>
          <input
            type="text"
            name="name"
            className="contact__form-input"
            placeholder="Type here"
          />
        </div>

        <div className="contact__form-div password-input-container">
  <label className="contact__form-tag">Password</label>
  <input
    type={showPassword ? 'text' : 'password'}
    name="password"
    className="contact__form-input"
    placeholder="Type here"
  />
  <div className="password-toggle" onClick={togglePasswordVisibility}>
    <FontAwesomeIcon
      icon={showPassword ? faEye : faEyeSlash}
      className="password-icon"
    />
  </div>
</div>

<div className="contact__form-div password-input-container">
  <label className="contact__form-tag">Confirm Password</label>
  <input
    type={showPassword ? 'text' : 'password'}
    name="confirmPassword"
    className="contact__form-input"
    placeholder="Type here"
  />
  <div className="password-toggle" onClick={togglePasswordVisibility}>
    <FontAwesomeIcon
      icon={showPassword ? faEye : faEyeSlash}
      className="password-icon"
    />
  </div>
</div>


        <div className="contact__form-div">
          <label className="contact__form-tag">Email Address</label>
          <input
            type="email"
            name="email"
            className="contact__form-input"
            placeholder="Type here"
          />
        </div>

        <div className="contact__form-div">
          <label className="contact__form-tag">Mobile Number</label>
          <input
            type="tel"
            name="phone"
            className="contact__form-input"
            placeholder="+63"
          />
        </div>

        <h3 className="contact__title2">Basic Information</h3>

        <div className="contact__form-div">
          <label className="contact__form-tag">First Name</label>
          <input
            type="text"
            name="basicUsername"
            className="contact__form-input"
            placeholder="Juan"
          />
        </div>

        <div className="contact__form-div">
          <label className="contact__form-tag">Middle Name</label>
          <input
            type="text"
            name="basicPassword"
            className="contact__form-input"
            placeholder="Cruz"
          />
        </div>

        <div className="contact__form-div">
          <label className="contact__form-tag">Last Name</label>
          <input
            type="text"
            name="basicConfirmPassword"
            className="contact__form-input"
            placeholder="Santiago"
          />
        </div>

        <div className="contact__form-div">
          <label className="contact__form-tag">Birthdate</label>
          <input
            type="date"
            name="basicEmail"
            className="contact__form-input"
            placeholder="Birthdate"
          />
        </div>

        <button className="button__ button--flex">NEXT</button>
      </form>
    </div>
  );
};

export default Register;