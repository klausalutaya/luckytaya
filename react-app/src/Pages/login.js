import React, { useState, useEffect } from 'react';
import { Button, Form, Container, InputGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import "../CSS/login.css";
import { motion } from "framer-motion";
import "./Iconsax/Svg/All/linear/"



const Login = () => {
  const [showPassword, setShowPassword] = useState(false);
  const [showForm, setShowForm] = useState(false);

  useEffect(() => {
    const timer = setTimeout(() => {
      setShowForm(true);
    }, 1000);

    return () => clearTimeout(timer);
  }, []);

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  return (
    <>
      <div className="container">
        <motion.div
          initial={{ y: 0 }}
          animate={{ y: -200 }}
          transition={{ duration: 0.7}}
        >
          <img
            className=""
            src="https://i.imgur.com/627iiL7.png"
            alt="Lucky Break"
          />
        </motion.div>
        <div className="billiards">
          <motion.img
            className="billiards__"
            src="https://i.imgur.com/6hCMhO8.png"
            alt="Billiards Image"
            initial={{ opacity: 1.8 }}
            animate={{ opacity: 0 }}
            transition={{ duration: 2 }}
          />
        </div>
        <Form style={{ width: "400px" }} className={`form ${showForm ? 'fade-in' : ''}`}>
          <Form.Group controlId="formBasicEmail">
            <Form.Control type="text" placeholder="Enter your username" className="userAndPassword" />
          </Form.Group>

          <Form.Group controlId="formBasicPassword" className="mt-3">
            <InputGroup>
              <Form.Control type={showPassword ? 'text' : 'password'} placeholder="Enter your password" className="userAndPassword" />
            </InputGroup>
          </Form.Group>

          <Form.Group className="mt-2 mb-4">
            <Form.Text className="text-muted">
              <Link to="/forgot-password" className="forgotPass">Forgot Password?</Link>
            </Form.Text>
          </Form.Group>

          <Button variant="primary" type="submit" className="button">
            LOGIN
          </Button>

          <Form.Group controlId="formBasicCheckbox" className="mt-3 d-flex justify-content-center">
            <Form.Check type="checkbox" label="Remember Me" className="rememberMe" />
          </Form.Group>
        </Form>
      </div>
    </>
  );
}

export default Login;
